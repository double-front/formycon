import retina from 'retinajs'
import './content-items/header/header'
import './content-items/header/burger'
import './content-items/carousel/carousel'
import './components/accordeon/accordeon'
import './content-items/tabs/tabs'
import './components/subscribe-form/subscribe-validation'
import './components/widget/complex-table'

$('body').on('click', 'a[href="#"]', e => e.preventDefault())

window.addEventListener('load', retina)
