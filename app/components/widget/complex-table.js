const tableCell = $('.fmc-complex-table > tbody td:nth-child(n+4)')

addTdClass()

function addTdClass() {
  tableCell.each((index, el) => {
    if ($(el).find('.fmc-progress').length !== 0) {
      $(el).prev().addClass('is-full')
    } else {
      $(el).prev().removeClass('is-full')
    }
  })
}
