import 'jquery-validation'

$('.js-subscribe-form').validate({
  rules: {
    email: {
      required: true,
      email: true
    }
  }
})
