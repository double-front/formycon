$('.accordeon__title').on('click', (e) => {
  const title = $(e.target)
  const media = title.data('accordeon-media')
  const content = title.next('.accordeon__content')
  const siblings = $('.accordeon__content')
  // console.log(media)
  if (!window.matchMedia(media).matches) return

  $('.accordeon__title').removeClass('accordeon__open')

  const visibleSiblings = siblings.filter(':visible')
  visibleSiblings.slideUp(400, () => {
    visibleSiblings.removeAttr('style')
  })

  if (!content.is(':visible')) {
    content.slideDown()
    title.addClass('accordeon__open')
  }
})

function resize() {
  const title = $('.accordeon__title')
  const media = title.data('accordeon-media')
  const siblings = $('.accordeon__content')
  const visibleSiblings = siblings.filter(':visible')
  if (!window.matchMedia(media).matches) {
    if (title.hasClass('accordeon__open')) {
      title.removeClass('accordeon__open')
      visibleSiblings.removeAttr('style')
    }
  }
}
// window.addEventListener('load', () => {
//   resize()
// }, false)
window.addEventListener('load resize orientationchange', resize, false)
