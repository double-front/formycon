import { TimelineMax } from 'gsap/all'
import Swiper from 'swiper/dist/js/swiper'

let tabSlider

let tabIndex = 0
let tabPrevIndex = tabIndex
const tabLink = $('.js-tab-link')
const tabContent = $('.js-tab-content')

const initTabSlider = (el) => {
  tabSlider = new Swiper(el, {
    speed: 500,
    slidesPerView: 1,
    pagination: {
      el: $(el).find('.swiper-pagination'),
      clickable: true
    }
  })
}

const destroyTabSlider = () => {
  tabSlider.destroy(true, true)
  tabSlider = undefined
}

$(tabContent[tabIndex]).addClass('is-visible')
$(tabLink[tabIndex]).addClass('is-active')

tabLink.on('click', (e) => {
  e.preventDefault()
  if ($(e.currentTarget).hasClass('is-active')) return
  tabIndex = $(e.currentTarget).index()

  tabLink.addClass('is-disabled')
  tabLink.removeClass('is-active')
  $(tabLink[tabIndex]).addClass('is-active')

  const tl = new TimelineMax({
    onComplete: () => {
      tabContent.removeClass('is-visible')
      if (!window.matchMedia('(min-width: 960px)').matches && tabSlider) {
        destroyTabSlider()
        console.log('destroy tab swiper on change')
      }
      $(tabContent[tabIndex]).addClass('is-visible')
      if (!window.matchMedia('(min-width: 960px)').matches && !tabSlider) {
        initTabSlider(tabContent[tabIndex])
        console.log('init tab swiper on change')
        console.log(tabSlider)
      }

      tabPrevIndex = tabIndex

      showTab(tabIndex)
    }
  })

  tl.staggerTo($(tabContent[tabPrevIndex]).find('.js-tab-box'), 0.25, {
    y: 40,
    opacity: 0
  }, 0.25)

  tabPrevIndex = tabIndex
})

function showTab(activeIndex) {
  const tl1 = new TimelineMax({
    onComplete: () => {
      tabLink.removeClass('is-disabled')
    }
  })
  tl1.staggerFromTo($(tabContent[activeIndex]).find('.js-tab-box'), 0.25, {
    y: 40,
    opacity: 0
  }, {
    y: 0,
    opacity: 1
  }, 0.175)
}


function resizeTab() {
  if (window.matchMedia('(min-width: 960px)').matches) {
    if (tabSlider) {
      destroyTabSlider()
      console.log('destroy tab swiper on resize')
    }
  } else {
    if (!tabSlider) {
      initTabSlider(tabContent[tabIndex])
      console.log('init tab swiper on resize')
    }
    console.log('initing')
  }
}

window.addEventListener('load', () => {
  resizeTab()
}, false)
window.addEventListener('resize', resizeTab, false)
