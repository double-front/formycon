export const isTouch = () => (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0))

export const clickTouch = () => {
  const e = isTouch() ? 'touchstart' : 'click'
  return e
}


const burger = $('.header__menu')
const $trigger = $('.header__burger-trigger')

const open = (e) => {
  e.stopPropagation()
  console.log('open', e.currentTarget)

  if (burger.hasClass('header__menu--open')) {
    closeOutside()
    return
  }

  $trigger.find('.hamburger-icon').addClass('hamburger-icon--open')
  burger.addClass('header__menu--open')
  $('body').addClass('fixed')
  $('body').on(clickTouch(), closeOutside)
}

function close() {
  console.log('closing')
  $trigger.find('.hamburger-icon').removeClass('hamburger-icon--open')
  burger.removeClass('header__menu--open')
  $('body').removeClass('fixed')
  $('body').off(clickTouch(), closeOutside)
}

function closeOutside(e = null) {
  const target = e ? $(e.target) : null

  if (
    !target ||
    (!target.hasClass('.header__menu') &&
      !target.closest('.header__menu, .header__burger-trigger').length)
  ) {
    close()
  }
}

$('.header__burger-trigger').on(clickTouch(), open)

function resize() {
  if (window.matchMedia('(min-width: 960px)').matches && burger.hasClass('header__menu--open')) {
    close()
  }
}

window.addEventListener('load', () => {
  resize()
}, false)
window.addEventListener('resize', resize, false)
