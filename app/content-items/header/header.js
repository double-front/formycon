import { clickTouch } from './burger'
import './fixed'

$('.header__link')
  .on('mouseenter', (e) => {
    const $that = $(e.currentTarget)

    $that
      .parent()
      .siblings()
      .find('.header__link')
      .removeClass('header__link--hover')
    $that.addClass('header__link--hover')
  })

$('.header__link-wrap').on('mouseleave', close)

function close() {
  const hovered = $('.header__link--hover')

  hovered.removeClass('header__link--hover')
  $('.header__link--active').addClass('header__link--hover')
}

function closeOutside(e = null) {
  const target = e ? $(e.target) : null

  if (
    !target ||
    (!target.hasClass('.header__link-wrap') &&
      !target.closest('.header__link-wrap').length)
  ) {
    close()
  }
}


$(document.body).on(clickTouch(), closeOutside)
