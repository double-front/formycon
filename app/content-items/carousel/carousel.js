import ScrollMagic from 'scrollmagic'
import Swiper from 'swiper/dist/js/swiper'

const controller = new ScrollMagic.Controller()
const $carousel = $('.carousel')
const $carouselWrap = $('.carousel__wrap')
const $carouselButtonNext = $('.carousel__button--next')
const $carouselItems = $('.carousel__item')
const height = $('.hero').height()

let scene
let swiper
let lockSwiper = false
let index = -1
const duration = 1500

const scrollmagicInit = () => {
  scene = new ScrollMagic.Scene({ offset: height - 208 }).addTo(controller)
    .on('enter', () => {
      $carousel.addClass('active')
    })
    .on('leave', () => {
      $carousel.removeClass('active')
    })
}

const swiperInit = (noSwiping) => {
  swiper = new Swiper('.carousel.swiper-container', {
    simulateTouch: false,
    direction: 'horizontal',
    loop: true,
    preventInteractionOnTransition: true,
    noSwiping,
    pagination: {
      el: $carousel.find('.swiper-pagination'),
      clickable: true
    }
  })
}

$carouselButtonNext.on('click', () => {
  if (lockSwiper) return
  // console.log($carouselItems, index)
  const nextIndex = index + 1 >= $carouselItems.length ? 0 : index + 1
  const $carouselItemPrev = $carouselItems.filter('.active')
  $carouselItemPrev
    .removeClass('active')
    .addClass('prev')
  const $carouselItemsActive = $carouselItems.eq(nextIndex)
  $carouselItemsActive
    .addClass('active')
    .removeClass('next')
  const $carouselItemNext = $carouselItemsActive.next().length
    ? $carouselItemsActive.next()
    : $carouselItems.first()
  $carouselItemNext.addClass('next')
  index = nextIndex

  // const nextIndex = swiper.realIndex + 1
  lockSwiper = true
  // console.log('lock')
  // swiper.slideToLoop(nextIndex)
  setTimeout(() => {
    lockSwiper = false
    $carouselItemPrev.removeClass('prev')
    // console.log('unlock')
  }, duration)
})

function resize() {
  if (window.matchMedia('(min-width: 960px)').matches) {
    if (scene) {
      scene = scene.destroy(true)
      console.log('destroy scene')
    }
    if (swiper) {
      swiper.destroy(true, true)
      console.log('destroy swiper')
    }
  } else {
    if (!scene) {
      scrollmagicInit()
      console.log('scene')
    }
    if (!swiper) {
      swiperInit(false)
      console.log('swiper')
    }
  }
}


window.addEventListener('load', () => {
  resize()
  $carouselButtonNext.trigger('click')
}, false)
window.addEventListener('resize orientationchange', resize, false)
