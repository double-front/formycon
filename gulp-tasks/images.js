module.exports = (gulp, {
  multipipe, imagemin, pngquant, notify
}, options) =>
  multipipe(
    gulp.src(options.src),
    imagemin([
      imagemin.jpegtran({ progressive: true }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: false }
        ]
      }),
      pngquant()
    ]),
    gulp.dest(options.dest)
  ).on('error', notify.onError(err => ({
    title: 'Clean Error',
    message: err.message
  })))
