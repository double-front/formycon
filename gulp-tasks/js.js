const webpackStream = require('webpack-stream')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const path = require('path')

module.exports = (gulp, plugins, options) => {
  const webpackPlugins = [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jquery: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ]

  if (options.isProd) {
    webpackPlugins.push(new UglifyJsPlugin({
      sourceMap: false
    }))
  }

  return plugins
    .multipipe(
      gulp.src(options.src, {}),
      plugins.eslint(),
      plugins.webpack(
        {
          entry: {
            // Index
            main: path.join(__dirname, '/../app/main'),
            polyfill: path.join(__dirname, '/../app/vendors/polyfill')
          },
          output: {
            publicPath: options.dest,
            filename: '[name].js'
          },
          watch: options.watch,
          module: {
            loaders: [
              {
                test: /\.js$/,
                exclude: [/vendors/, /(node_modules|bower_components)/],
                loaders: ['babel-loader?presets[]=es2015', 'eslint-loader']
              }
            ]
          },
          plugins: webpackPlugins,
          watchOptions: {
            aggregateTimeout: 200,
            poll: true
          }
        },
        webpack
      ),
      gulp.dest(options.dest)
    )
    .on(
      'error',
      plugins.notify.onError(err => ({
        title: 'CSS',
        message: err.message
      }))
    )
}
